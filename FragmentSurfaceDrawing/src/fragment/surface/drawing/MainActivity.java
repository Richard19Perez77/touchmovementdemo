package fragment.surface.drawing;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import fragment.surface.fragment.DrawingSurface;

/**
 * Starting point for application, defines layout in the activity_main file.
 * 
 * @author Rick
 * 
 * Git created https://Richard19Perez77@bitbucket.org/Richard19Perez77/touchmovementdemo.git
 *
 */
public class MainActivity extends ActionBarActivity {

	/**
	 * Set the layout from XML file.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	/**
	 * Create menu options that should be visible regardless of fragments menu
	 * options added.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * On menu item selected provide a way to handle each items selection.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public DrawingSurface getDrawingSurface() {
		DrawingSurface drawingSurface;
		drawingSurface = (DrawingSurface) findViewById(R.id.drawingSurface);
		return drawingSurface;
	}
}